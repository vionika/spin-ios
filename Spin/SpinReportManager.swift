//
//  Copyright © 2016 National Education Technologies Inc. All rights reserved.
//

import Foundation

class SpinReportManager {
    let parentServerURL: String?
    let deviceToken: String?
    
    
    init() {
        // Load server url from app group preferences
        let defaults = UserDefaults(suiteName: "group.com.nationaledtech.spin")
        self.parentServerURL = defaults?.value(forKey: "PARENT_SERVER_URL") as? String
        self.deviceToken = defaults?.value(forKey: "DEVICE_TOKEN") as? String
        
        // Observe history notifications
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector:#selector(onLocationChange), name:NSNotification.Name(rawValue: "OnLocationChange"), object: nil)
    }
    
    @objc func onLocationChange(notification: NSNotification) {
        let url = notification.userInfo!["url"] as? NSURL
        let title = notification.userInfo!["title"] as? String
        
        if url != nil && title != nil {
            self.postHistoryURL(url: url!, title: title!)
        }
    }
    
    private func postHistoryURL(url: NSURL, title: String) {
        guard self.parentServerURL != nil && self.deviceToken != nil else {
            return
        }
        
        let defaults = UserDefaults.standard
        
        if (defaults.value(forKey: "TRACK_HISTORY") == nil) {
            return
        }
        
        let trackHistory = defaults.value(forKey: "TRACK_HISTORY") as! Bool
        
        if !trackHistory {
            return
        }
        
        let postURLString = "https://" + self.parentServerURL! + "/services/browserservice.svc/ReportVisitedUrl"
        var request = URLRequest(url: URL(string: postURLString)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        
        let json = ["DeviceToken" : self.deviceToken!,
                    "Url": url.absoluteString!,
                    "TimeStamp": String(Date.nowMicroseconds() / 1000),
                    "Title": title] as Dictionary<String, String>
        
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        request.httpBody = jsonData;
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
        }
        task.resume()
    }
}
