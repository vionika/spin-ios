//
//  SafetyManager.swift
//  Client
//
//  Created by Yuri Chernikov on 2015-12-17.
//  Copyright © 2015 Mozilla. All rights reserved.
//

import Foundation


class SafetyManager {
    private var apiToken: String? = nil
    
    private var linkDetector: NSDataDetector? = nil;
    private var regexGoogle: NSRegularExpression? = nil
    private var regexBing: NSRegularExpression? = nil
    private var regexYahoo: NSRegularExpression? = nil
    private var regexDuckduckgo: NSRegularExpression? = nil
//    private var prohibitedCategories: [Int] = [1010, 1011, 1027, 1031, 1058, 1062, 1076, 10002, 10005, 10007]
    private var prohibitedCategories: [Int] = [1011, 1062, 10002, 10007, 1031, 1058, 10005]
    private let DefaultTimeoutLengthInNanoSeconds: UInt64 = 10*1000000000
    private let SpinBrowseDomain: String = "spinbrowse.com"
    private let LocalhostDomain: String = "localhost"
    private let DefaultApiKey: String = "oz2erssx768cHfzDMOO1PsyIz2EaJsyDppqrwmHckoHsrGBOJ2tPkA=="
    private let CachePrevix: String = "DomainCategory#:"
    private let BlockedPageTemplate: String = "http://www.spinbrowse.com/blocked/?url=%@&domain=%@&category=%d&managed=%@&os=iOS"
    internal static let Allowed: Int = 1;
    
    private var blackList: Array<String>?
    private var whiteList: Array<String>?
    
    internal static let instance = SafetyManager()
    
    private var cache: Dictionary = Dictionary<String, [Int]>()
    
    init() {
        do {
            let types: NSTextCheckingResult.CheckingType = [.link]
            self.linkDetector = try? NSDataDetector(types: types.rawValue)
            
            self.regexGoogle = try NSRegularExpression(pattern:"^(https?:\\/\\/)?(w{3}\\.)?(images\\.)?google\\.", options: NSRegularExpression.Options.caseInsensitive)
            self.regexBing = try NSRegularExpression(pattern:"^(https?:\\/\\/)?(w{3}\\.)?(.+\\.)?bing\\.", options: NSRegularExpression.Options.caseInsensitive)
            self.regexYahoo = try NSRegularExpression(pattern:"^(https?:\\/\\/)?(w{3}\\.)?(.+\\.)?yahoo\\.", options: NSRegularExpression.Options.caseInsensitive)
            self.regexDuckduckgo = try NSRegularExpression(pattern:"^(https?:\\/\\/)?(w{3}\\.)?(.+\\.)?duckduckgo\\.", options: NSRegularExpression.Options.caseInsensitive)
            self.loadPolicies()
            
            NotificationCenter.default.addObserver(self, selector:#selector(loadPolicies), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        } catch {
            print(error)
        }
    }
    
    func safeProcessRequest(request: URLRequest,  completionHandler handler: @escaping (String, Int) -> Void) -> URLRequest {
        var updatedRequest = request;
        if (request.url != nil) {
            var urlString = request.url!.absoluteString
            
            if (urlString == "spin-parental-controls-ios") {
                urlString = "http://nationaledtech.com/spin-parental-controls-ios/"
            }
            
            let domainName = getDomainName(urlString)
            if (!domainName!.hasSuffix(SpinBrowseDomain) && !domainName!.hasSuffix(LocalhostDomain)) {
                urlString = processSafeSearch(urlString)
                
                //if (!self.isSearchEngineUrl(urlString!)) {
                let domainCategory = verifyUrl(url: urlString, completionHandler: handler)
                if (domainCategory != SafetyManager.Allowed) {
                    urlString = self.buildBlockedUrl(url: urlString, category: domainCategory)
                } else {
                    // Check if params contain restricted url
                    let urlComponents = NSURLComponents(string: urlString)!
                    let paramUrls = parseUrlsFromParams(queryItems: urlComponents.queryItems)
                    for paramUrl in paramUrls {
                        let domainCategory = verifyUrl(url: paramUrl, completionHandler: handler)
                        if (domainCategory != SafetyManager.Allowed) {
                            urlString = self.buildBlockedUrl(url: paramUrl, category: domainCategory)
                            break
                        }
                    }
                }
                //}
            }
            
            let url = URL(string: urlString)
            updatedRequest = PrivilegedRequest(url: url!) as URLRequest
        }
        
        return updatedRequest;
    }
    
    func processAddressChange(_ tab: Tab) -> Void {
        processAddressChange(tab, url: tab.url?.absoluteString);
    }
    
    func processAddressChange(_ tab: Tab, url: String?) -> Void {
        if (url == nil || !self.verifyUrl(url)) {
            return
        }
        
        let domainName = getDomainName(url!)
        if (domainName != nil && !domainName!.hasSuffix(SpinBrowseDomain) && !domainName!.hasSuffix(LocalhostDomain) && !isWhitelistUrl(url!)) {
            let safeSearchUrl = processSafeSearch(url!)
            
            if (safeSearchUrl != url) {
                tab.loadSafeRequest(PrivilegedRequest(url: URL(string: safeSearchUrl)!) as URLRequest)
                return
            }
            
            //if (!self.isSearchEngineUrl(safeSearchUrl)) {
            func safetyManagerCallback(url: String, category: Int) -> Void {
                if (category != SafetyManager.Allowed) {
                    blockPage(tab: tab, url: url, category: category)
                }
            }
            
            let category = SafetyManager.instance.verifyUrl(url: url!, completionHandler: safetyManagerCallback)
            safetyManagerCallback(url: url!, category: category)
            
            // Check if params contain restricted url
            let urlComponents = NSURLComponents(string: url!)!
            let paramUrls = parseUrlsFromParams(queryItems: urlComponents.queryItems)
            for paramUrl in paramUrls {
                let category = SafetyManager.instance.verifyUrl(url: paramUrl, completionHandler: safetyManagerCallback)
                safetyManagerCallback(url: paramUrl, category: category)
            }
            //}
        }
    }
    
    func parseUrlsFromParams(queryItems:[URLQueryItem]?) -> [String] {
        guard let params = queryItems  else {
            return []
        }
        
        var result:[String] = [];
        for queryItem in params {
            if verifyUrl(queryItem.value) {
                result.append(queryItem.value!)
            }
        }
        
        return result
    }
    
    func verifyUrl(_ urlString: String?) -> Bool {
        guard let string = urlString else {return false}
        
        guard (self.linkDetector != nil && string.count > 0) else { return false }
        
        if self.linkDetector!.numberOfMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, string.count)) > 0 {
            return true
        }
        return false
    }
    
    func buildBlockedUrl(url: String, category: Int) -> String {
        let domainName = self.getDomainName(url)
        
        let defaults = UserDefaults(suiteName: "group.com.nationaledtech.spin")
        let parentServerURL = defaults?.value(forKey: "PARENT_SERVER_URL") as? String
        let deviceToken = defaults?.value(forKey: "DEVICE_TOKEN") as? String
        let managedValue = (parentServerURL != nil && deviceToken != nil) ? "true" : "false"
        
        return String(format: BlockedPageTemplate,
                      url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!, domainName!, category, managedValue);
    }
    
    func verifyUrl(url: String,  completionHandler handler: @escaping (String, Int) -> Void) -> Int {
        var domainCategory = SafetyManager.Allowed
        if (!url.isEmpty) {
            let domainName = getDomainName(url)
            
            if (domainName != nil && !domainName!.hasSuffix(SpinBrowseDomain) && !domainName!.hasSuffix(LocalhostDomain)) {
                // Check for managed configuration black list
                if let managedBlackList = SpinDefaults.standart.blackList {
                    for (_, element) in managedBlackList.enumerated() {
                        let elementDomainName = getDomainName(element) ?? ""
                        if domainName == elementDomainName || domainName!.hasSuffix("." + elementDomainName) {
                            return 0
                        }
                    }
                }
                
                // Check for managed configuration white list
                if let managedWhiteList = SpinDefaults.standart.whiteList {
                    for (_, element) in managedWhiteList.enumerated() {
                        let elementDomainName = getDomainName(element) ?? ""
                        if domainName == elementDomainName || domainName!.hasSuffix("." + elementDomainName) {
                            return SafetyManager.Allowed
                        }
                    }
                }
                
                // Check for Boomerang white list
                if self.whiteList != nil {
                    for (_, element) in self.whiteList!.enumerated() {
                        if domainName == element || domainName!.hasSuffix("." + element) {
                            return SafetyManager.Allowed
                        }
                    }
                    
                    return 0
                }
                
                // Check for boomerang black list
                if self.blackList != nil {
                    for (_, element) in self.blackList!.enumerated() {
                        if domainName == element || domainName!.hasSuffix("." + element) {
                            return 0
                        }
                    }
                }
                
                // Check for categories
                //let cachedValue: Bool? = self.cache.objectForKey(domainName!) as! Bool?
                let cachedCategories: [Int]? = self.retrieveFromCache(domainName: domainName!)
                
                if (cachedCategories != nil) {
                    //let allowed = self.areAllowedDomainCategories(cachedCategories!);
                    let prohibitedCategory = self.getFirstProhibitedCategory(categories: cachedCategories!)
                    handler(url, prohibitedCategory)
                    return prohibitedCategory
                } else {
                    let sem = DispatchSemaphore(value: 0)
                    let backgroundQueue = DispatchQueue.global(qos: .background)
                    backgroundQueue.async { [unowned self] () -> Void in
                        self.checkDomainCategory(domainName: domainName!, completionHandler: {(prohibited: Int) -> Void in
                            handler(url, prohibited)
                            domainCategory = prohibited
                            sem.signal()
                        });
                    }
                    
                    let timeout = DispatchTime.now() + .seconds(5)
                    if sem.wait(timeout: timeout) == .timedOut {
                        print("\(url) timed out")
                    }
                }
            }
        }
        return domainCategory
    }
    
    func processSafeSearch(_ url: String) -> String {
        var updatedUrl = url;
        
        // Check if Google
        var match = self.regexGoogle!.numberOfMatches(in: url,
                                                      options:[],
                                                      range:NSRange(location:0,
                                                                    length:url.count));
        if (match > 0 && !url.contains("safe=strict") && !url.contains("/maps/") && !url.contains("/amp/") && !url.contains(".js") && !url.contains(".html")) {
            if (url.contains("?") || url.contains("#")) {
                updatedUrl = url + "&safe=strict";
            } else {
                updatedUrl = url + "?safe=strict";
            }
        }
        
        // Check if Bing
        match = self.regexBing!.numberOfMatches(in: url,
                                                options:[],
                                                range:NSRange(location:0,
                                                              length:url.count));
        if (match > 0 && !url.contains("adlt=strict") && !url.contains(".js") && !url.contains(".html")) {
            if (url.contains("?")) {
                updatedUrl = url + "&adlt=strict";
            } else {
                updatedUrl = url + "?adlt=strict";
            }
        }
        
        // Check if Yahoo
        match = self.regexYahoo!.numberOfMatches(in: url,
                                                 options:[],
                                                 range:NSRange(location:0,
                                                               length:url.count));
        if (match > 0 && !url.contains("vm=r") && !url.contains(".js") && !url.contains(".html")) {
            if (url.contains("?")) {
                updatedUrl = url + "&vm=r";
            } else {
                updatedUrl = url + "?vm=r";
            }
        }
        
        // Check if Duckduckgo
        match = self.regexDuckduckgo!.numberOfMatches(in: url,
                                                      options:[],
                                                      range:NSRange(location:0,
                                                                    length:url.count));
        if (match > 0 && !url.contains("kp=1") && !url.contains(".js") && !url.contains(".html")) {
            if (url.contains("?")) {
                updatedUrl = url + "&kp=1";
            } 
        }
        
        return updatedUrl;
    }
    
    func checkDomainCategory(domainName: String, completionHandler handler: @escaping (Int) -> Void) -> [Int] {
        let result = [Int]();
        
        let requestUrl = URL(string: "http://www.vionika.com/services/examine/domain")
        //        let request = NSURLRequest(URL: url!)
        var request = URLRequest(url: requestUrl!)
        //        var session = NSURLSession.sharedSession()
        request.httpMethod = "POST"
        
        let apiKey = self.apiToken != nil ? self.apiToken! : DefaultApiKey
        
        let params = ["domainName":domainName, "key":apiKey, "v":"1"] as Dictionary<String, String>
        
        
        let queue:OperationQueue = OperationQueue()
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted);
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            NSURLConnection.sendAsynchronousRequest(request, queue: queue) {(response, data, error) in
                do {
                    if (data == nil) {
                        return;
                    }
                    let jsonResult: NSDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    let categories: [Int] = jsonResult.value(forKey: "categories") as! [Int]
                    self.putIntoCache(domainName: domainName, categories: categories);
                    
                    //let allowed = self.areAllowedDomainCategories(categories);
                    let prohibitedCategory = self.getFirstProhibitedCategory(categories: categories);
                    
                    //self.cache.setObject(allowed, forKey:domainName)
                    handler(prohibitedCategory);
                } catch {
                    print(error);
                }
                
                
            }
        } catch {
            print(error);
        }
        return result;
    }
    
    func isSearchEngineUrl(url: String) -> Bool {
        let googleMatch = self.regexGoogle!.numberOfMatches(in: url,
                                                            options:[],
                                                            range:NSRange(location:0, length:url.count))
        let yahooMatch = self.regexYahoo!.numberOfMatches(in: url,
                                                          options:[],
                                                          range:NSRange(location:0, length:url.count))
        let bingMatch = self.regexBing!.numberOfMatches(in: url,
                                                        options:[],
                                                        range:NSRange(location:0, length:url.count))
        let duckduckgo = self.regexDuckduckgo!.numberOfMatches(in: url,
                                                               options:[],
                                                               range:NSRange(location:0, length:url.count))
        
        return googleMatch != 0 || yahooMatch != 0 || bingMatch != 0 || duckduckgo != 0
    }
    
    func isWhitelistUrl(_ url: String) -> Bool {
        // check if twitter plugin
        if url.starts(with: "https://platform.twitter.com/widgets/") {
            return true
        }
        
        // check if embed youtube
        if url.starts(with: "https://www.youtube.com/embed/") {
            return true
        }
        
        // check if Google captcha
        if url.starts(with: "https://www.google.com/recaptcha/") {
            return true
        }
        
        return false
    }
    
    func blockPage(tab: Tab, url: String, category: Int) {
        DispatchQueue.main.async() { [unowned self] in
            if (category != SafetyManager.Allowed) {
                print("[SafetyManager] Blocking request")
                
                let blockedUrlString = self.buildBlockedUrl(url: url, category: category);
                tab.loadRequest(PrivilegedRequest(url: URL(string: blockedUrlString)!) as URLRequest)
            }
        }
    }
    
    func putIntoCache(domainName: String, categories: [Int]) {
        self.cache[CachePrevix + domainName] = categories
    }
    
    func retrieveFromCache(domainName: String) -> [Int]? {
        return self.cache[CachePrevix + domainName] 
    }
    
    func areAllowedDomainCategories(categories: [Int]) -> Bool {
        let prohibitedCategory = getFirstProhibitedCategory(categories: categories);
        return prohibitedCategory == SafetyManager.Allowed
    }
    
    func getFirstProhibitedCategory(categories: [Int]) -> Int {
        // Check for managed prohibited categories if exist, if no use spin categories
        if let managedProhibitedCategories = SpinDefaults.standart.prohibitedCategories {
            for cat in categories {
                if managedProhibitedCategories.contains(cat) {
                    return 0
                }
            }
        } else {
            for cat in categories {
                if prohibitedCategories.contains(cat) {
                    return cat
                }
            }
        }
        
        return SafetyManager.Allowed
    }
    
    func getDomainName(_ url: String) -> String? {
        var domainName = NSURL(string: url)?.host
        if domainName != nil && domainName!.hasPrefix("www.") {
            domainName = domainName!.replacingOccurrences(of: "www.", with: "")
        }
        
        return domainName
    }
    
    @objc private func loadPolicies() {
        let defaults = UserDefaults(suiteName: "group.com.nationaledtech.spin")
        let parentServerURL = defaults?.value(forKey: "PARENT_SERVER_URL") as? String
        let deviceToken = defaults?.value(forKey: "DEVICE_TOKEN") as? String
        
        guard parentServerURL != nil && deviceToken != nil else {
            return
        }
        
        let postURLString = "https://" + parentServerURL! + "/services/browserservice.svc/GetBrowsingPolicies";
        let request = NSMutableURLRequest(url: URL(string: postURLString)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        
        let json = ["DeviceToken" : deviceToken!];
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        request.httpBody = jsonData;
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            do {
                let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                
                if let dictionary = responseDictionary {
                    self.apiToken = dictionary["ApiToken"] as? String
                    
                    if self.apiToken == nil {
                        let defaults = UserDefaults.standard
                        defaults.set(false, forKey:"TRACK_HISTORY")
                        defaults.synchronize()
                        
                        self.whiteList = nil
                        self.blackList = nil
                        
                        return;
                    }
                    
                    let policies: Array<Dictionary<String, AnyObject>> = (dictionary["Policies"] as? Array<Dictionary<String, AnyObject>>)!
                    
                    let defaults = UserDefaults.standard
                    defaults.set(policies.count > 0, forKey:"TRACK_HISTORY")
                    defaults.synchronize()
                    
                    for (_, element) in policies.enumerated() {
                        let policyDictionary: Dictionary = element
                        if policyDictionary["Type"] as! NSNumber == 40 {
                            self.whiteList = nil
                            self.blackList = nil
                            
                            let contentArray: NSArray = (policyDictionary["Content"] as? NSArray)!
                            var domens: Array<String> = Array<String>()
                            for (_, item) in contentArray.enumerated() {
                                let contentDict = try JSONSerialization.jsonObject(with: (item as! String).data(using: String.Encoding.utf8)!, options: []) as? [String:String]
                                let domain: String = (contentDict?["Domain"])!
                                domens.append(domain)
                            }
                            
                            if policyDictionary["SubType"] as! NSNumber == 0 { // Black list
                                self.blackList = domens
                            }
                            else if policyDictionary["SubType"] as! NSNumber == 1 { // White list
                                self.whiteList = domens
                            }
                        }
                        else if policyDictionary["Type"] as! NSNumber == 47 {
                            if let categoriesData = (policyDictionary["Properties"]! as! String).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
                                do {
                                    let categoriesDict = try JSONSerialization.jsonObject(with: categoriesData, options: []) as? [String:[Int]]
                                    self.prohibitedCategories = (categoriesDict?["Prohibited"])!
                                } catch let error as NSError {
                                    print(error)
                                }
                            }
                        }
                    }
                }
            } catch let error as NSError {
                print(error)
            }
        }
        task.resume()
    }
}
