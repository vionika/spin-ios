//
//  SpinDefaults.swift
//  Copyright © 2018 Mozilla. All rights reserved.
//

import Foundation
import Storage
import Shared

class SpinDefaults {
    static let standart = SpinDefaults()
    
    private(set) public var newTabType:NewTabPage?
    private(set) public var newTabTypeEnforced:Bool?
    
    private(set) public var homePageUrl:String?
    private(set) public var homePageEnforced:Bool?
    
    private(set) public var showSuggestions:Bool?
    private(set) public var searchEngine:String?
    
    private(set) public var prohibitedCategories:Array<Int>?
    
    private(set) public var bookmarks:Array<BookmarkItem>?
    
    private(set) public var blackList:Array<String>?
    private(set) public var whiteList:Array<String>?
    
    private init() {
        NotificationCenter.default.addObserver(self, selector: #selector(userDefaultsDidChange), name: UserDefaults.didChangeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UserDefaults.didChangeNotification, object: nil)
    }
    
    @objc private func userDefaultsDidChange(_ notification: Notification) {
        loadDefaults()
    }
    
    func loadDefaults() {
        resetDefaults()
        
        if let managedConfigDict:Dictionary<String, Any> = UserDefaults.standard.dictionary(forKey: "com.apple.configuration.managed") {
            // Parse new tab
            if let newTabValue = managedConfigDict[caseInsensitive: "new tab"] {
                if let newTabDictionary = newTabValue as? Dictionary<String, Any> {
                    if let newTabTypeValue = newTabDictionary[caseInsensitive: "type"] {
                        if let value = newTabTypeValue as? String {
                            switch value.lowercased() {
                            case "topsites":
                                newTabType = NewTabPage.topSites
                            case "blank":
                                newTabType = NewTabPage.blankPage
                            case "bookmarks":
                                newTabType = NewTabPage.bookmarks
                            case "history":
                                newTabType = NewTabPage.history
                            case "homepage":
                                newTabType = NewTabPage.homePage
                            default: break
                            }
                        }
                    }
                    
                    if newTabType != nil, let newTabEnforceValue = newTabDictionary[caseInsensitive: "enforced"] {
                        newTabTypeEnforced = newTabEnforceValue as? Bool
                    }
                }
            }
            
            // Parse home page settings
            if let homePageValue = managedConfigDict[caseInsensitive: "home page"] {
                if let homePageDictionary = homePageValue as? Dictionary<String, Any> {
                    if let homePageUrlValue = homePageDictionary[caseInsensitive: "url"] {
                        homePageUrl = homePageUrlValue as? String
                    }
                    
                    if homePageUrl != nil, let homePageEnforceValue = homePageDictionary[caseInsensitive: "enforced"] {
                        homePageEnforced = homePageEnforceValue as? Bool
                    }
                }
            }
            
            // Parse suggestions
            if let searchEngineValue = managedConfigDict[caseInsensitive: "search engine"] {
                if let searchEngineDictionary = searchEngineValue as? Dictionary<String, Any> {
                    if let suggestionsValue = searchEngineDictionary[caseInsensitive: "show search suggestions"] {
                        showSuggestions = suggestionsValue as? Bool
                    }
                }
            }
            
            // Parse prohibited categories
            if let prohibitedCategoriesValue = managedConfigDict[caseInsensitive: "prohibited categories"] {
                prohibitedCategories = prohibitedCategoriesValue as? Array<Int>
            }
            
            // Parse bookmarks
            if let bookmarksValue = managedConfigDict[caseInsensitive: "bookmarks"] {
                parseBookmarksData(bookmarksValue as? Array<Dictionary<String, String>>)
            }
            
            // Parse black list
            if let blackListValue = managedConfigDict[caseInsensitive: "black list"] {
                blackList = blackListValue as? Array<String>
            }
            
            // Parse white list
            if let whiteListValue = managedConfigDict[caseInsensitive: "white list"] {
                whiteList = whiteListValue as? Array<String>
            }
        }
    }
    
    private func parseBookmarksData(_ bookmarksData: Array<Dictionary<String, String>>?) {
        guard bookmarksData != nil else {
            return
        }
        
        var bookmarks = Array<BookmarkItem>()
        
        for bookmarkDictionary in bookmarksData! {
            let guid = Bytes.generateGUID()
            let title = bookmarkDictionary[caseInsensitive: "title"] ?? ""
            let url = bookmarkDictionary[caseInsensitive: "url"] ?? ""
            let faviconUrl = bookmarkDictionary[caseInsensitive: "favicon"] ?? ""
            
            let bookmark = BookmarkItem(guid: guid, title: title, url: url)
            bookmark.favicon = Favicon(url: faviconUrl, date: cacheFaviconDate())
            bookmarks.append(bookmark)
        }
        
        self.bookmarks = bookmarks
    }
    
    private func resetDefaults() {
        newTabType = nil
        newTabTypeEnforced = nil
        
        homePageUrl = nil
        homePageEnforced = nil
        
        showSuggestions = nil
        searchEngine = nil
        
        prohibitedCategories = nil
        
        bookmarks = nil
        
        blackList = nil
        whiteList = nil
    }
    
    private func cacheFaviconDate() -> Date {
        var components = DateComponents()
        components.year = 2018
        components.month = 9
        components.day = 28
        
        // Get NSDate given the above date components
        return (NSCalendar(identifier: NSCalendar.Identifier.gregorian)?.date(from: components))!
    }
}
