//
//  SafetyHelper.swift
//  Client
//
//  Created by Yuri Chernikov on 2016-01-04.
//  Copyright © 2016 Mozilla. All rights reserved.
//

import Foundation
import WebKit

class SafetyHelper: NSObject, TabContentScript {
    private weak var tab: Tab?
    weak var delegate: BrowserSafetyDelegate?
    private var isRemoved:Bool = false
    
    struct Elements {
        let link: URL?
        let image: URL?
    }
    
    class func name() -> String {
        return "SafetyHelper"
    }

    required init(tab: Tab) {
        super.init()
        self.tab = tab
        
        let path = Bundle.main.path(forResource: "ContextMenu", ofType: "js")!
        let source = try! NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue) as String
        let userScript = WKUserScript(source: source, injectionTime: WKUserScriptInjectionTime.atDocumentEnd, forMainFrameOnly: false)
        tab.webView!.configuration.userContentController.addUserScript(userScript)
    }
    
    func scriptMessageHandlerName() -> String? {
        return "safetyMessageHandler"
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {

        // This code fires blocking page during google search scrolling, looks like it legacy and no longer needed, left it in case commenting may cause any regression
        
//        let data = message.body as! [String: AnyObject]
//        
//        var linkURL: URL?
//        if let urlString = data["link"] as? String {
//            linkURL = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.URLAllowedCharacterSet())!)
//        }
//        
//        var imageURL: URL?
//        if let urlString = data["image"] as? String {
//            imageURL = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.URLAllowedCharacterSet())!)
//        }
//        
//        if linkURL != nil || imageURL != nil {
//            let urlToCheck = linkURL != nil ? linkURL : imageURL;
//            if (urlToCheck != nil) {
//                SafetyManager.instance.processAddressChange(tab!, url: urlToCheck?.absoluteString);
//            } 
//        }
    }
}
