//
//  BrowserSafety.swift
//  Client
//
//  Created by Yuri Chernikov on 2016-01-04.
//  Copyright © 2016 Mozilla. All rights reserved.
//

import Foundation

@objc
protocol BrowserSafetyDelegate: class {
    func closeSafetyTab(tab: Tab)
}